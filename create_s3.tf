#Aws access information
provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region = var.aws_region
}

#creating a private bucket w tags
resource "aws_s3_bucket" "b" {
  bucket = var.aws_bucket_name
  acl = "private"
  tags = {
    Name = var.aws_bucket_alias
  }
}


