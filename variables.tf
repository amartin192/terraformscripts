variable "aws_region" {}

variable "aws_access_key" {}
variable "aws_secret_key" {}

variable "aws_bucket_name" {}
variable "aws_bucket_alias" {}
